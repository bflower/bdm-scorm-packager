# bdm-scorm-packager

scorm packager for b-eden MROCS modules


## Installation

npm i @b-flower/bdm-scorm-packager

## Commands

````
  init|i -> create param object for packager and store it in scormPackager key package.json
  make|m -> create scorm archive based on config scormPackager in package.json
````

## Initialization
Initialization (cmd line) :

````
  $ npx bdm-sco-builder init
````

## Packager

Script in package.json

````json
  ...
  "pack": "bdm-sco-builder make"
  ...
````

Then in command line

````
  $ npm run pack
````
