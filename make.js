const path = require('path')
const scp = require('simple-scorm-packager')
const pkg = require('./lib/package')

module.exports = make

function nowDate() {
  const now = new Date()
  return (
    now.getFullYear() +
    '-' +
    ('0' + (now.getMonth() + 1)).slice(-2) +
    '-' +
    ('0' + now.getDate()).slice(-2)
  )
}

const MERGE_CONF = {
  date: nowDate(),
  timestamp: Date.now(),
  version: pkg.get().version,
  status: 'final',
}

function getPkgConf() {
  return pkg.get()[pkg.KEY]
}

function mergeConf() {
  const conf = getPkgConf()
  conf.source = path.resolve(conf.source)
  conf.package = { ...conf.package, ...MERGE_CONF }
  return conf

}

function make() {
  return scp(mergeConf())
}
