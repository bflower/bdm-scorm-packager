#!/usr/bin/env node
const path = require('path')
const inquirer = require('inquirer')
const utils = require(path.join(__dirname, 'lib', 'utils.js'))
const pkg = require('./lib/package')


const generateDefaultSettings = require(path.join(
  __dirname,
  'lib',
  'schemas',
  'config.js'
))

const validators = {
  notEmpty(value) {
    if (value && utils.cleanAndTrim(value) !== '') return true
    return 'Please enter a valid information'
  },
  number(value) {
    return !isNaN(parseFloat(value)) || 'Please enter a number'
  },
}

const settings = {
  version: '2004 3rd Edition',
  language: 'fr',
  organization: 'company_name',
  title: pkg.get().name || '',
  identifier: '',
  masteryScore: 80,
  startingPage: 'index.html',
  source: 'dist',
  package: {
    zip: true,
  },
}

function trim(v) {
  return v.trim()
}
function trimLow(v) {
  return trim(v).toLowerCase()
}

function cliInit() {
  return inquirer
    .prompt([
      {
        type: 'list',
        name: 'version',
        message: 'Select scorm version:',
        choices: ['2004 3rd Edition', '1.2', '2004 4th Edition'],
      },
      {
        type: 'input',
        name: 'title',
        message: 'Select title:',
        default: settings.title,
        validate: validators.notEmpty,
        filter: trim,
      },
      {
        type: 'input',
        name: 'organization',
        message: 'Select organization:',
        default: settings.organization,
        validate: validators.notEmpty,
        filter: trim,
      },
      {
        type: 'input',
        name: 'language',
        message: 'Select language:',
        default: settings.language,
        validate: validators.notEmpty,
        filter: trimLow,
      },
      {
        type: 'input',
        name: 'masteryScore',
        message: 'Select mastery score [Range 0-100]:',
        default: settings.masteryScore,
        filter: Number,
        validate: validators.number,
      },
      {
        type: 'input',
        name: 'startingPage',
        message: 'Select starting page:',
        default: settings.startingPage,
        validate: validators.notEmpty,
        filter: trim,
      },
    ])
    .then(input => {
      settings.version = input.version
      settings.title = input.title
      settings.organization = input.organization
      settings.language = input.language
      settings.masteryScore = input.masteryScore
      settings.startingPage = input.startingPage
    })
}

function cliIdentifer() {
  return inquirer
    .prompt([
      {
        type: 'input',
        name: 'identifier',
        message: 'Select an identifier:',
        default: (utils.acronym(settings.organization) + '00').toUpperCase(),
        validate: validators.notEmpty,
        filter: trim,
      },
    ])
    .then(input => {
      settings.identifier = input.identifier
    })
}

function cliSource() {
  return inquirer
    .prompt([
      {
        type: 'input',
        name: 'from',
        default: 'dist',
        filter: trim,
        message: 'Select source directory:',
      },
    ])
    .then(input => {
      settings.source = input.from || settings.source
    })
}

module.exports = function() {
  return cliInit()
    .then(cliIdentifer)
    .then(cliSource)
    .then(() => {
      const pkgJson = pkg.get()
      const config = generateDefaultSettings(settings, pkgJson)

      pkgJson.scormPackager = config
      return pkg.save(pkgJson)
    })
    .then(() => {
      console.log('Saved to package.json')
    })
    .catch(e => {
      console.error('Error during bdm-scorm-publish-cmd param generator')
      console.error(e)
    })
}
