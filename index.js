#!/usr/bin/env node
const path = require('path')
const program = require('commander')
const pkg = require(path.join(__dirname, 'package.json'))

const init = require('./init')
const make = require('./make')

program
  .version(pkg.version)
  .description('SCORM packager')
  .usage('hello')

program
  .command('init')
  .alias('i')
  .description(
    'create param object for packager and store it in scormPackager key package.json'
  )
  .action(() => {
    return init()
  })

program
  .command('make')
  .alias('m')
  .description(
    'create scorm archive based on config scormPackager in package.json'
  )
  .action(() => {
    return make()
  })

program.on('--help', () => {
  console.log('')
  console.log('Initialization (cmd line) :')
  console.log('  $ npx bdm-sco-builder init')
  console.log('')
  console.log('Script in package.json')
  console.log('  "pack": "bdm-sco-builder make"')
})

// error on unknown commands
program.on('command:*', () => {
  console.error(
    'Invalid command: %s\nSee --help for a list of available commands.',
    program.args.join(' ')
  )
  process.exit(1)
})

program.parse(process.argv)

if (program.args.length === 0) {
  program.help()
}
