const path = require('path')
const utils = require('../utils')
const timestamp = Date.now()

function stringToArray(value) {
  if (Array.isArray(value)) {
    return value
  }

  if (typeof value === 'string') {
    const arr = []
    if (value) arr.push(value)
    return arr
  }

  return Object.values(value)
}


module.exports = function(obj, pkg) {
  if (!obj.package) obj.package = {}
  return {
    version: obj.version || '1.2',
    uuid: utils.uuid(timestamp),
    language: obj.language || 'fr',
    organization: obj.organization || '',
    title: obj.title || pkg.name,
    identifier: obj.identifier,
    masteryScore: obj.masteryScore !== null ? obj.masteryScore : 80,
    startingPage: obj.startingPage || 'index.html',
    source: obj.source || '.',
    package: {
      name: obj.package.name || obj.title || pkg.name || '',
      author: obj.package.author || pkg.author || '',
      description:
        obj.package.description || obj.description || pkg.description || '',
      organization: obj.package.organization || obj.organization || '',
      outputFolder: obj.package.outputFolder || './scorm',
      duration: obj.package.duration || 'PT0H10M0S',
      typicalDuration:
        obj.package.typicalDuration || obj.package.duration || 'PT0H10M0S',
      educational:
        obj.package.educational ||
        obj.package.description ||
        obj.description ||
        '',
      rights: obj.package.rights || pkg.license || '',
      requirements: obj.package.requirements || [],
      keywords: stringToArray(obj.package.keywords || ''),
      status: obj.package.status || 'final',
      zip: obj.package.zip,
    },
  }
}
